import React, {Component} from 'react';
import ToDo from "./AddTaskForm/AddTaskForm";
import Task from "./Task/Task";
import fire from '../firebase';
import Spinner from "../Spinner/Spinner";


class Todolist extends Component {

  state = {
    task: [],
    inputValue: 'Add new task',
    loading: false
  };
  componentWillMount() {
    this.UpdateBase()
  }


  UpdateBase = () => {
    this.setState({loading: true});
    /* Create reference to data in Firebase Database */
    let messagesRef = fire.database().ref('task').orderByKey().limitToLast(100);
    messagesRef.on('child_added', snapshot => {
      /* Update React state when message is added at Firebase Database */
      let message = snapshot.val();
      message.key = snapshot.key;
      this.setState({task: [message].concat(this.state.task)});

    });
    this.setState({loading: false});
  };
  addTask = () => {
    const taskText = this.state.inputValue;
    let taskId = Date.now();
    let task = {text: taskText,id: taskId};
    const inputValue = 'Add new task';
    fire.database().ref('task').push(task);
    this.setState({inputValue});
  };

  change = (event) => {
    let inputValue = [...this.state.inputValue];
    inputValue = event.target.value;
    this.setState({inputValue});
  };

  deleteTask = (id) => {
    const tasks = [...this.state.task];
    const index = tasks.findIndex(p => p.id === id);

    let key = tasks[index].key;
    tasks.splice(index,1);
    fire.database().ref('task').child(key).remove();

    this.setState({task: tasks});
  };

  checkTask = (id) => {
    const tasks = [...this.state.task];
    const index = tasks.findIndex(p => p.id === id);
    tasks[index].status = !tasks[index].status;
    this.setState({tasks});
  };

  render() {



   let tasks = (
      <div className="items">
        {this.state.task.map((task) => {
          let status = null;
          if(task.status) status = 'fa fa-check-square-o';
          else status = 'fa fa-square-o';
          return <Task
            key={task.id}
            text={task.text}
            status={status}
            check={() => this.checkTask(task.id)}
            delete={() => this.deleteTask(task.id)}
          />
        })}
      </div>
    );
    if (this.state.loading) {
      tasks = <Spinner />;
    }

    return (
      <div className="main">
        <div className="container">
          <ToDo
            value={this.state.inputValue}
            add={() => this.addTask()}
            change={(event) => this.change(event)}
          />
          {tasks}
        </div>
      </div>
    );
  }
}

export default Todolist;
