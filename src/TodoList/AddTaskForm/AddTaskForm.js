import React from 'react';
import './AddTaskForm.css';

const ToDo = props => {
	return (
		<header>
			<input className='input' onChange={props.change} value={props.value} type="text"/>
			<button onClick={props.add}>ADD</button>
		</header>
	)
};

export default ToDo;