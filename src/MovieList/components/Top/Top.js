import React from 'react';

const Top = props => {
	return (
		<div className="top">
			<input onChange={props.changeName} className="name" type="text" value={props.name}/>
			<button onClick={props.addItem} className="add">Add</button>
		</div>
	)
};

export default Top;