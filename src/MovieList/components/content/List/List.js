import React from 'react';
import Listing from "./List/Listing";

const List= props => {
  if(!props.name){return null}

	return (
		<div>
      {
        props.name.map(item => {
          return <Listing
            key={item.id}
            name={item.name}
            deleteItem={props.deleteItem}
            editItem={props.editItem}
            findItem={props.findItem}
            id={item.id}
          />;
        })
      }
		</div>
	)
};

export default List;