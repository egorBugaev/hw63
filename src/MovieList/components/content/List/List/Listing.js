import React from 'react';

const Listing = props => {
	return (
		<div className="items-list">
            <input value={props.name} onChange={(event) => props.editItem(event)}
                      onClick={() => props.findItem(props.id)}
            />
			<button onClick={() => props.deleteItem(props.id)} className="delete">
				Delete
			</button>
		</div>
	)
};

export default Listing;