import React, {Component} from 'react'
import Top from "./components/Top/Top";
import Content from "./components/content/content";
import fire from '../firebase';
import Spinner from "../Spinner/Spinner";


class MovieList extends Component {

  state = {
    moviesList: [],
    name: '',
    pos: '',
    loading: false
  };

  componentWillMount() {
    this.setState({loading: true});
    this.UpdateBase();
    this.setState({loading: false});
  }


  UpdateBase = () => {

    /* Create reference to data in Firebase Database */
    let messagesRef = fire.database().ref('moviesList').orderByKey().limitToLast(100);
    messagesRef.on('child_added', snapshot => {
      /* Update React state when message is added at Firebase Database */
      let message = snapshot.val();
      message.key = snapshot.key;
      this.setState({moviesList: [message].concat(this.state.moviesList)});

    });

  };

  changeName = (event) => {
    let name = this.state.name;
    name = event.target.value;
    this.setState({name});
  };

  addItem = (e) => {
    e.preventDefault();
    let moviesList = [...this.state.moviesList];
    if (!this.state.moviesList) {
      moviesList = [];
    }
    let name = this.state.name;
    let item = {
      id: Date.now(),
      name: name,
    };
    moviesList.push(item);
    name = '';
    this.setState({name});
    fire.database().ref('moviesList').push(item);
  };

  deleteItem = (id) => {
    const moviesList = [...this.state.moviesList];
    const index = moviesList.findIndex(p => p.id === id);
    let key = moviesList[index].key;
    fire.database().ref('moviesList').child(key).remove();

    moviesList.splice(index, 1);
    this.setState({moviesList});
  };

  findItem = (id) => {
    const moviesList = [...this.state.moviesList];
    let pos = moviesList.findIndex(p => p.id === id);
    this.setState({pos});
  };
  editItem = (event) => {
    const moviesList = [...this.state.moviesList];
    moviesList[this.state.pos].name = event.target.value;
    this.setState({moviesList});
    localStorage.setItem('moviesList', JSON.stringify(moviesList));
  };

  render() {
    let list= <Content
      movies={this.state.moviesList}
      deleteItem={this.deleteItem}
      findItem={this.findItem}
      editItem={this.editItem}
    />;
    if (this.state.loading) {
      list = <Spinner />;
    }
    return (
      <div className="list">
        <Top
          changeName={(event) => this.changeName(event)}
          addItem={(e) => this.addItem(e)}
          name={this.state.name}
        />
        {list}
      </div>
    )
  }

}

export default MovieList;