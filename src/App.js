import React, { Component, Fragment } from 'react';
import './App.css';
import MovieList from "./MovieList/MovieList";
import {Switch, Route, NavLink, BrowserRouter} from "react-router-dom";
import Todolist from "./TodoList/Todo";


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
        <header className="App-header">
          <NavLink exact to="/">MovieList</NavLink>
          <NavLink to="/TodoList">ToDoList</NavLink>
        </header>

        <Switch>
          <Route path="/" exact component={MovieList}/>
          <Route path="/TodoList" component={Todolist}/>
          <Route render={()=> <h1>404 NOT FOUND</h1>}/>
        </Switch>
        </Fragment>
      </BrowserRouter>
    );
  }
}

export default App;



